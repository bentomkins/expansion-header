import { Directive, ElementRef, Renderer2 } from '@angular/core';

@Directive({
  selector: '[noSelect]'
})
export class NoSelectDirective {
  domElement: any;
  constructor(
    private renderer: Renderer2,
    private elementRef: ElementRef
  ) {
    this.domElement = this.elementRef.nativeElement;

    const userSelectStyles: any = {
      '-webkit-touch-callout': 'none',
      '-webkit-user-select': 'none',
      '-khtml-user-select': 'none',
      '-moz-user-select': 'none',
      '-ms-user-select': 'none',
      'user-select': 'none',
    };

    Object.keys(userSelectStyles).forEach(style => {
      this.renderer.setStyle(
        this.domElement, `${style}`, userSelectStyles[style]
      );
    });
  }

}
