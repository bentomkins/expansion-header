import { HostListener } from '@angular/core';
import { Directive } from '@angular/core';

@Directive({
  selector: '[appStopPropagation]'
})
export class StopPropagationDirective {

  @HostListener('click', ['$event'])
  public onClick(event: any): void
  {
      event.stopPropagation();
  }

}
