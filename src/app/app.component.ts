import { Component } from '@angular/core';
import { ViewChild } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  @ViewChild('panel') panel: any;
  expanded = false;

  togglePanel(event: MouseEvent): void {
    this.expanded = !this.expanded;
  }

  clickBlock(event: MouseEvent): void {
    event.stopPropagation();
    if (this.expanded) { this.panel.open(); }
    if (!this.expanded) { this.panel.close(); }
  }


}
